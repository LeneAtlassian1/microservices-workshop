package ${package}.notification.domains;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Valid
@Data
@NoArgsConstructor
public class Notification {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    private String number;

    @NotBlank
    private String agency;

    @NotNull
    private BigDecimal availableCreditLimit;

    private String text;

    public String getText() {
        return "Seja bem vindo ao Sensedia Bank " +
                StringUtils.capitalize(this.getFirstName()) + " " +
                StringUtils.capitalize(this.getLastName()) + "! \n" +
                "Dados da sua conta: \n" +
                "Número: *" + this.getNumber() + "* \n" +
                "Agência: *" + this.getAgency() + "* \n" +
                "Limite: *" + this.getAvailableCreditLimit().toString() + "*";
    }
}
